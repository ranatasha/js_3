'use strict';

function start() {
    let count = prompt("Сколько фильмов вы уже посмотрели?", "");
    console.log(count);
    while (count == "" || count == null || isNaN(count) ) {
        count = prompt("Сколько фильмов вы уже посмотрели?", "");
    }

    count = Number(count);
    return count;
}

function rememberMyFilms(object) {
    for (let i = 0; i < 2; i++) {
        const lastWatchedFilm = prompt("Один из последних просмотренных фильмов?", ""),
              filmRating = prompt("На сколько оцените его?", "");

        if (lastWatchedFilm != null && filmRating != null
            && lastWatchedFilm != "" && filmRating != "" 
            && lastWatchedFilm.length <= 50 && filmRating.length <=50)  {
            object.movies[lastWatchedFilm] = filmRating;
        } else {
            console.log("the user entered the data incorrectly");
            i--;
        }
    }
}

function detectPersonalLevel(counter) {
    if (counter < 10) {
        alert("Просмотрено довольно мало фильмов");
    } else if (counter >= 10 && counter <=30) {
        alert("Вы классический зритель");
    } else if (counter > 30) {
        alert("Вы киноман");
    } else {
        console.log("Произошла ошибка");
    }
}

function main() {
    const numberOfFilms = start();

    const personalMovieDB = {
        count: numberOfFilms,
        movies: {},
        actors: {},
        genres: [],
        privat: false
    };

    rememberMyFilms(personalMovieDB);
    detectPersonalLevel(personalMovieDB.count);

    console.log(personalMovieDB);
}

main();


// Код возьмите из предыдущего домашнего задания
